# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

PACKAGE_PATH = pathlib.Path(__file__).parent

# == IRC settings ==
IRC_SERVER = env.str("IRC_SERVER", default="irc.libera.chat")
IRC_PORT = env.int("IRC_PORT", default=6697)
IRC_USE_SSL = env.bool("IRC_USE_SSL", default=True)
IRC_USE_PASSWORD = env.bool("IRC_USE_PASSWORD", default=False)
IRC_USER = env.str("IRC_USER", default="wikibugs")
IRC_NICK = env.str("IRC_NICK", default="wikibugs")
IRC_PASSWORD = env.str("IRC_PASSWORD")
IRC_AUTOJOINS = env.list("IRC_AUTOJOINS", default=["#wikimedia-cloud"])
IRC_SAL_CHANNEL = env.str("IRC_SAL_CHANNEL", default="#wikimedia-cloud")
IRC_SAL_PROJECT = env.str("IRC_SAL_PROJECT", default="tools.wikibugs")
IRC_CHANNELS_FILE = env.str(
    "IRC_CHANNELS_FILE",
    default=PACKAGE_PATH / "channels.yaml",
)

# == Phorge settings ==
INVALID_PHAB_TOKEN = "api-badcafe012345678901234567890"
PHAB_HOST = env.str("PHAB_HOST", default="https://phabricator.wikimedia.org")
PHAB_TOKEN = env.str("PHAB_TOKEN", default=INVALID_PHAB_TOKEN)

# == Redis settings ==
REDIS_HOST = env.str(
    "REDIS_HOST",
    default="redis.svc.tools.eqiad1.wikimedia.cloud",
)
REDIS_QUEUE_NAME = env.str("REDIS_QUEUE_NAME", default="wikibugs2")

# == Gerrit settings ==
GERRIT_SERVER = env.str("GERRIT_SERVER", default="gerrit.wikimedia.org")
GERRIT_PORT = env.int("GERRIT_PORT", default=29418)
GERRIT_SSH_USER = env.str("GERRIT_SSH_USER", default="suchabot")
GERRIT_SSH_KEY = env.str("GERRIT_SSH_KEY", multiline=True)
GERRIT_CHANNELS_FILE = env.str(
    "GERRIT_CHANNELS_FILE",
    default=PACKAGE_PATH / "gerrit-channels.yaml",
)

# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import asyncio
import json
import logging
import socket

import asyncio_redis
import asyncio_redis.encoders
import irc3

from . import channelfilter
from . import messagebuilder
from . import settings

__version__ = "3.0alpha"
current_host = socket.getfqdn()
logger = logging.getLogger(__name__)


class Redis2Irc(irc3.IrcBot):
    logging_config = {"version": 1, "incremental": True}

    def __init__(self, builder, chanfilter, **kwargs):
        """
        :type builder: messagebuilder.PhorgeMessageBuilder
        :type chanfilter: channelfilter.ChannelFilter
        """
        self.channels = set(kwargs["autojoins"])
        super().__init__(**kwargs)
        self._builder = builder
        self._chanfilter = chanfilter

    def join_many(self, targets):
        for target in targets:
            if target not in self.channels:
                self.join(target)

    def join(self, target):
        super().join(target)
        self.channels.add(target)

    def part(self, target, reason=None):
        super().part(target, reason)
        if target in self.channels:
            self.channels.remove(target)

    async def privmsg_many(self, targets, message):
        for target in targets:
            await self.privmsg(target, message)

    async def privmsg(self, target, message, nowait=False):
        # if target not in self.channels:
        #     self.join(target)
        self.join(target)  # FIXME HACK
        await super().privmsg(target, message, nowait=nowait)

    @property
    def builder(self):
        return self._builder

    @property
    def chanfilter(self):
        return self._chanfilter


async def handle_useful_info(bot, useful_info):
    """
    :type bot: Redis2Irc
    :type useful_info: dict
    """
    ignored = (
        "gerritbot",
        "ReleaseTaggerBot",
        "Stashbot",
        "Phabricator_maintenance",
        "CommunityTechBot",
        "Maintenance_bot",
    )
    if useful_info["user"] in ignored:
        # Ignore some Phabricator bots
        logger.info("Skipped %(url)s by %(user)s", useful_info)
        return
    updated = bot.chanfilter.update()
    if updated:
        await bot.privmsg(
            settings.IRC_SAL_CHANNEL,
            f"!log {settings.IRC_SAL_PROJECT} Updated channels.yaml to: {updated}",
        )
        logger.info("Updated channels.yaml to: %s", updated)

    channels = bot.chanfilter.channels_for(
        {project: None for project in useful_info["projects"]},
    )
    for chan, matched_projects in channels.items():
        useful_info["channel"] = chan
        useful_info["matched_projects"] = matched_projects
        text = bot.builder.build_message(useful_info)
        await bot.privmsg(chan, text)


async def redisrunner(bot):
    """
    :type bot: Redis2Irc
    """
    while True:
        try:
            await redislistener(bot)
        except Exception:
            logger.exception(
                "Redis listener crashed; restarting in a few seconds.",
            )
        await asyncio.sleep(5)


async def redislistener(bot):
    """
    :type bot: Redis2Irc
    """
    # Create connection
    connection = await asyncio_redis.Connection.create(
        host=settings.REDIS_HOST,
        port=6379,
    )

    while True:
        try:
            future = await connection.blpop([settings.REDIS_QUEUE_NAME])
        except Exception:
            logger.exception("Redis configuration failed; retrying.")
        else:
            useful_info = json.loads(future.value)
            if useful_info.get("raw"):
                # Send a pre-formatted Gerrit message.
                bot.create_task(
                    bot.privmsg_many(
                        useful_info["channels"],
                        useful_info["msg"],
                    ),
                )
            else:
                # Create and send a Phorge message.
                bot.create_task(handle_useful_info(bot, useful_info))


def main():
    """Read messages from redis, format them, and send them to irc."""
    chanfilter = channelfilter.ChannelFilter(path=settings.IRC_CHANNELS_FILE)

    plugins = [
        "irc3.plugins.core",
        "irc3.plugins.ctcp",
        "irc3.plugins.autojoins",
        "irc3.plugins.log",
        __name__,  # register this module as a plugin
    ]
    # SASL auth is preferred when connecting directly to an IRC network, but
    # when connecting via a BNC service password auth may be required.
    password = None
    sasl_username = settings.IRC_USER
    sasl_password = settings.IRC_PASSWORD
    if settings.IRC_USE_PASSWORD:
        password = settings.IRC_PASSWORD
        sasl_username = None
        sasl_password = None
        logger.info(f"Configured for password auth to {settings.IRC_SERVER}")
    else:
        plugins.append("irc3.plugins.sasl")
        logger.info(f"Configured for SASL auth as {sasl_username}")

    bot = Redis2Irc(
        builder=messagebuilder.PhorgeMessageBuilder(),
        chanfilter=chanfilter,
        nick=settings.IRC_NICK,
        autojoins=settings.IRC_AUTOJOINS,
        host=settings.IRC_SERVER,
        port=settings.IRC_PORT,
        ssl=settings.IRC_USE_SSL,
        username=settings.IRC_USER,
        password=password,
        sasl_username=sasl_username,
        sasl_password=sasl_password,
        realname=(
            "Wikibugs v2.1, <https://www.mediawiki.org/wiki/Wikibugs> "
            + "running on "
            + current_host
        ),
        includes=plugins,
        ctcp={
            "finger": "{realname}",
            "source": "{realname}",
            "version": "{realname}",
            "userinfo": "{realname}",
            "ping": "PONG",
        },
    )

    bot.create_task(redisrunner(bot))
    bot.run()

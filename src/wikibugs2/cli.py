# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import logging

import click
import coloredlogs

from .gerrit import main as gerrit_main
from .irc import main as irc_main
from .phorge import main as phorge_main
from .tools.update_credits import main as update_credits_main
from .version import __version__
from .wblogging import PrivateTimedRotatingFileHandler

logger = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.option(
    "--logfile",
    default=None,
    type=click.Path(dir_okay=False),
    help="Log to this (rotated) log file",
)
@click.pass_context
def wikibugs2(ctx, verbose, logfile):
    """IRC announce bot for issue tracker and forge events."""
    ctx.ensure_object(dict)
    ctx.obj["VERBOSE"] = verbose
    ctx.obj["LOGFILE"] = logfile

    log_fmt = "%(asctime)s %(name)s %(levelname)s: %(message)s"
    log_datefmt = "%Y-%m-%dT%H:%M:%SZ"
    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt=log_fmt,
        datefmt=log_datefmt,
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    if logfile:
        handler = PrivateTimedRotatingFileHandler(
            logfile,
            when="midnight",
            backupCount=7,
            utc=True,
            encoding="utf-8",
        )
        handler.setLevel(logging.INFO)
        if verbose >= 3:
            # T359230: don't send debug level to disk until `-vvv`
            handler.setLevel(logging.DEBUG)
        handler.setFormatter(
            logging.Formatter(fmt=log_fmt, datefmt=log_datefmt),
        )
        logging.getLogger().addHandler(handler)
        click.echo(f"Logging to {logfile} with daily rotation.")

    logger.debug(f"Invoking {ctx.invoked_subcommand}")


@wikibugs2.command()
@click.pass_context
def gerrit(ctx):
    """Process Gerrit event-stream events and enqueue IRC messages."""
    return gerrit_main(verbosity=ctx.obj["VERBOSE"])


@wikibugs2.command()
@click.option(
    "--ask/--no-ask",
    default=False,
    help="Ask before pushing change to Redis",
)
@click.option(
    "--start-from",
    default=0,
    help="chronologicalKey value to start polling from.",
)
@click.argument(
    "files",
    type=click.Path(exists=True),
    nargs=-1,
)
def phorge(ask, start_from, files):
    """Process Phorge events and enqueue data for creating IRC messages.

    If files are provided, parse them as XACT files instead of polling Phorge
    """
    return phorge_main(
        ask_before_push=ask,
        start_from=start_from,
        files=files,
    )


@wikibugs2.command()
def irc():
    """Read messages from redis, format them, and send them to irc."""
    return irc_main()


@wikibugs2.command()
def update_credits():
    """Update CREDITS file with current contributors."""
    return update_credits_main()


if __name__ == "__main__":  # pragma: nocover
    wikibugs2()

# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
"""IRC announce bot for bug tracker and code forge events."""
from . import cli
from .version import __version__  # noqa: F401 imported but unused

if __name__ == "__main__":  # pragma: nocover
    cli.wikibugs2()

# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import asyncio
import json
import time

import asyncio_redis
import redis


class RedisQueue:
    def __init__(self, name, host="localhost"):
        self.redis = redis.StrictRedis(host=host, port=6379, db=0)
        self.key = name
        self.last_pushed = 0

    def put(self, item):
        """Put item into the queue."""
        delaytime = 2
        diff = time.time() - self.last_pushed
        if diff < delaytime:
            time.sleep(delaytime - diff)
        self.redis.rpush(self.key, json.dumps(item))
        self.last_pushed = time.time()

    def get(self):
        """Remove and return an item from the queue.

        Will block until an item is available"""
        item = self.redis.blpop(self.key)
        if item:
            return json.loads(item[1].decode())
        return item


class AsyncRedisQueue:
    """Queue shaped wrapper around an asyncio_redis connection."""

    @classmethod
    async def create(
        cls,
        queue_name,
        host="localhost",
        port=6379,
        poolsize=1,
        min_push_delay=0,
    ):
        """Create a new async Redis queue.

        :param queue_name: Name of the Redis list that implements the queue
        :param host: Redis host
        :param port: Redis port
        :param poolsize: Number of pooled connections to use
        :param min_push_delay: minimum delay to enforce between push attempts
        """
        self = cls()
        self.key = queue_name
        self.last_push_time = 0
        self.min_push_delay = min_push_delay
        self.redis = await asyncio_redis.Pool.create(
            host=host,
            port=port,
            poolsize=poolsize,
        )
        return self

    async def put(self, item):
        """Put item into the queue."""
        if self.min_push_delay:
            diff = time.time() - self.last_push_time
            if diff < self.min_push_delay:
                await asyncio.sleep(self.min_push_delay - diff)
            self.last_push_time = time.time()
        await self.redis.rpush(self.key, [json.dumps(item)])

    async def get(self):
        """Remove and return an item from the queue."""
        item = await self.redis.blpop(self.key)
        if item:
            return json.loads(item[1].decode())
        return item

    def close(self):
        """Close connection to Redis."""
        return self.redis.close()

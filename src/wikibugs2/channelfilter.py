# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import collections
import logging
import os
import pprint
import re
import subprocess
import time

import yaml

logger = logging.getLogger(__name__)


class ChannelFilter:
    def __init__(self, path):
        self.path = path
        self.time = 0
        self.mtime = 0
        self.config = {}
        self.load()

    def load(self):
        with open(self.path, encoding="utf-8") as f:
            self.config = yaml.safe_load(f)
        self.compile_channels()
        self.time = time.time()
        self.mtime = os.path.getmtime(self.path)
        logger.info(pprint.pformat(self.config))

    def compile_channels(self):
        """Compile configuration into more optimal runtime structure."""
        checks = collections.defaultdict(dict)
        for channel, patterns in self.config["channels"].items():
            for pattern, filters in patterns.items():
                matcher = re.compile(
                    pattern,
                    flags=re.IGNORECASE | re.UNICODE,
                )
                checks[channel][matcher] = filters
        self.config["channels"] = checks

    @property
    def firehose_channel(self):
        return self.config["firehose-channel"]

    @property
    def default_channel(self):
        return self.config["default-channel"]

    def all_channels(self):
        channels = {
            self.default_channel,
            self.firehose_channel,
        }
        channels.update(self.config["channels"].keys())
        return [c for c in channels if c.startswith("#")]

    def update(self):
        if time.time() - self.time > 60:
            mtime = os.path.getmtime(self.path)
            if mtime != self.mtime:
                self.load()
                cwd = os.getcwd()
                desc = False
                try:
                    os.chdir(os.path.dirname(self.path))
                    desc = subprocess.check_output(
                        [
                            "git",
                            "rev-list",
                            "HEAD",
                            "--max-count=1",
                            "--pretty=oneline",
                        ],
                    )
                finally:
                    os.chdir(cwd)
                return desc.decode().strip()
        return False

    def channels_for(self, inputs, selector=None):
        """Get all channels that should be notified about the inputs.

        :param inputs: Inputs to check
        :type inputs: dict[label: attributes]
        :param selector: Selector function to test matching inputs
        :type selector: callable
        :returns: dict[channel: list(labels)]
        """
        channels = collections.defaultdict(list)
        for channel, patterns in self.config["channels"].items():
            for matcher, filters in patterns.items():
                for label, attributes in inputs.items():
                    if matcher.fullmatch(label):
                        if filters is None:
                            channels[channel].append(label)
                        elif selector is None:
                            channels[channel].push(label)
                        elif selector(label, attributes, filters):
                            channels[channel].append(label)

        if not channels:
            channels[self.default_channel] = []
        channels.pop("/dev/null", None)
        channels[self.firehose_channel] = []

        return channels

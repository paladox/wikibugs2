# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
from logging.handlers import TimedRotatingFileHandler
import os


def private_open(file, flags, dir_fd=None):
    """Open a file with ``u=rw,g=,o=`` permissions."""
    return os.open(file, flags, mode=0o600, dir_fd=dir_fd)


class PrivateTimedRotatingFileHandler(TimedRotatingFileHandler):
    """Rotate log files at timed intervals with ``u=rw,g=,o=`` permissions."""

    def _open(self):
        return open(
            self.baseFilename,
            self.mode,
            encoding=self.encoding,
            opener=private_open,
        )

#!/bin/bash
set -euox pipefail

COMPONENT=${1:?usage: $0 COMPONENT}
echo $(date --iso-8601=second) -- Starting $COMPONENT

PYTHON=${VENV:=${HOME}/venv-wikibugs2-39}/bin/python3

exec ${PYTHON} -m wikibugs2 \
    -vv \
    --logfile ${HOME}/logs/${COMPONENT}.log \
    ${COMPONENT}

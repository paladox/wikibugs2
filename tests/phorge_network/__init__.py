# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
from collections import OrderedDict
import json
import urllib.parse


def parse_request(request):
    decoded = OrderedDict(urllib.parse.parse_qsl(request.text))
    assert decoded["output"] == "json"
    return json.loads(decoded["params"])


def conduit_connect(request, context):  # noqa: U100 Ununsed argument
    return {"result": {"sessionKey": "key", "connectionID": 1}}


def unexpected(request, context):  # noqa: U100 Ununsed argument
    raise Exception("Received unexpected request: " + request)

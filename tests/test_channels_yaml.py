# Copyright (c) 2014-2015 Kunal Mehta, YuviPanda and Merlijn van Deen
# Copyright (c) 2024 Wikimedia Foundation and contributors.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file or at https://opensource.org/licenses/MIT.
import wikibugs2.channelfilter
import wikibugs2.settings


def test_phorge_config():
    chanfilter = wikibugs2.channelfilter.ChannelFilter(
        wikibugs2.settings.IRC_CHANNELS_FILE,
    )
    # An exception would have been raised if that wasn't the case
    print("channels.yaml has valid syntax")

    assert {"#mediawiki-feed", "#wikimedia-releng"} == set(
        chanfilter.channels_for({"Release-Engineering-Team": None}),
    )
    assert {"#mediawiki-feed", "#wikimedia-dev", "#wikimedia-releng"} == set(
        chanfilter.channels_for({"Phabricator": None}),
    )
    assert {
        "#mediawiki-feed",
        "#wikimedia-collaboration",
        "#pywikibot",
    } == set(chanfilter.channels_for({"Pywikibot-Flow": None}))
